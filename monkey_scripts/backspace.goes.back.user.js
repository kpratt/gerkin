// ==UserScript==
// @name          Keynan's Chrome broke my backspace script.
// @namespace     http://duality-studios.ca
// @description   enables backslash == back button
// @include       *
// @require       http://code.jquery.com/jquery.min.js
// ==/UserScript==


$(window).keypress(function(e){
  var code = e.keyCode || e.which;
  var active = document.activeElement.nodeName;
  if(code == 92 && //high jack back slash cause backspace doesn't generate a keyCode !!!
      !(active == 'textarea' ||
        active == 'input'
       )
  ){
    history.back(1); 
  }
});

