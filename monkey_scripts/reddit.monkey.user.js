// ==UserScript==
// @name          Keynan's Reddit helper
// @namespace     http://duality-studios.ca
// @description   enables advanced tabing on reddit
// @include       *.reddit.com/*
// @require       file:///home/keynan/workspaces/gerkin/assets/jquery.min.js
// @require       file:///home/keynan/workspaces/gerkin/assets/gerkin.js
// @resource      targetingStyles file:///home/keynan/workspaces/gerkin/assets/targeting.css
// ==/UserScript==

(function($) {
//  $(function(){

    function spaceClick(e){
      var code = e.keyCode || e.which;
      if(code == 32){
        this.click();
      }
    }
  
    $('.linkflair').find('a.title').keydown(spaceClick);
    $('.linkflair').find('a.title').keydown(spaceClick);
    $('#siteTable').find('.thing').find('.arrow').keydown(spaceClick);
  
  
    if($('.last-clicked').length == 0){
      $('a.title:link').first().addClass('target-first');
    } else {
      var unvisited = $('.last-clicked').next().next();
      if(unvisited.length == 0){
        var target = $('.nextprev').find("a:contains('next')");
        target.addClass('target-first');
      } else {
        unvisited.find('a.title:link').first().addClass('target-first');
      }
    }
  
    gerkin.navigate_with_keyboard({
      strategy:'spacial nav', //   | "flow tree"
      target_class:'aui-tab-target',
      auto_apply_targeting:function () {
        var elems = $('body, a.title, .linkflair, .arrow.up, .arrow.down, .nextprev a');
        elems.attr('tab-index', 0);
        return elems;
      }
    });

    $('head').append('<style type="text/css">' + GM_getResourceText('targetingStyles') + '</style>');

    var x = $('.target-first');
    $('html, body').animate({
            scrollTop: (x.position().top - 130) + 'px'
        }, 'fast');
//  });
})($);
