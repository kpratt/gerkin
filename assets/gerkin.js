function GerKiN($) {

  var highlight_focused_element = true;
    var highlight_border_width = 2;

  function visual_debug(select){
    $.each(select(), function(i, x){
      var elem = $(x);
      console.log(x.tagName +'#'+ elem.attr('id'));
      $('<div id="debug-gh-'+i+'"></div>').appendTo($("body"));
      var gh = $('#debug-gh-'+i);
      gh.css("display","block");
      gh.css("background","purple");
      gh.css("position","absolute");
      gh.css("border","purple solid 2px");
      
    
      gh.css("top",    elem.offset().top);
      gh.css("left",   elem.offset().left);
      gh.css("width",  8);
      gh.css("height", 8);
      gh.css("z-index", 99999999);
    });
  }

    
  function filter(array, memberFn) {
    var ret = [];
    $.each(array, function() {
      if (memberFn($(this))) {
        ret.push(this);
      }
    });
    return ret;
  };

  function getHorizontalCenter(e) {
    var gpos = e.data('GerKiN_Pos');
    if (gpos == undefined) {
      return e.offset().left + (e.width() / 2);
    } else {
      return gpos.x;
    }
  }
  function getVerticalCenter(e) {
    var gpos = e.data('GerKiN_Pos');
    if (gpos == undefined) {
      return e.offset().top + (e.height() / 2);
    } else {
      return gpos.y;
    }
  }
  function getLeft(e) {
    if(e.offset == undefined){
      return null;
    }
    if(e.offset() == undefined){
      return null;
    }
    return e.offset().left;
  }
  function getTop(e) {
    return e.offset().top;
  }

  var getXPos = getLeft;
  var getYPos = getTop;
  function getPosition(e) {
    return {
      x : getXPos(e),
      y : getYPos(e)
    };
  };

  function highlight(elem) {
    if(!highlight_focused_element || elem.length == 0){
      return;
    }
    var highlighter = $("#gerkin-highlighter");
    if(highlighter.length == 0){
      $('<div id="gerkin-highlighter"></div>').appendTo($("body"));
      highlighter = $("#gerkin-highlighter");
      highlighter.css("display","none");
      highlighter.css("background","transparent");
      highlighter.css("position","absolute");
      highlighter.css("border","#2CFF3A solid 2px");
      highlighter.css("pointer-events", 'none');
    }
    var t = elem.offset().top;
    var l = elem.offset().left;
    var w = elem.outerWidth();
    var h = elem.outerHeight();
    highlighter.css("top",   t - highlight_border_width);
    highlighter.css("left",  l - highlight_border_width);
    highlighter.css("height",h);
    highlighter.css("width", w);
    highlighter.css("display", "block");
    
    //border rounding
    highlighter.css("border-radius", elem.css("border-radius"));
  }
  
  var focusing = false;
  function giveFocus(focusOn) {
    if(!focusing){
      focusing = true;
      focusOn = $(focusOn);
      
      var hadFocus = $('.hasFocus')
      
      hadFocus.removeClass('hasFocus');
      focusOn.addClass('hasFocus');
      hadFocus.off('focus', giveFocusEvent);
      focusOn.off('focus', giveFocusEvent);
      hadFocus.blur();
      if(focusOn[0] == hadFocus[0] || (focusOn[0].nodeName != 'TEXTAREA' && focusOn[0].nodeName != 'INPUT')) {
        focusOn.focus();
      }
      focusOn.on('focus', giveFocusEvent);
      hadFocus.on('focus', giveFocusEvent);
      
      highlight(focusOn);
      console.log(focusOn);
      focusing = false;
    }
  };
  function giveFocusEvent(event){giveFocus($(event.currentTarget));};
  
  return {
    keys: {
      'tab'   : 9,
      'up'    : 87,
      'down'  : 83,
      'right' : 68,
      'left'  : 65
    },
    navigate_with_keyboard : function(settings) {
      var g = this;

      function handleNav(nav) {
        return function(e) {
          var code = e.keyCode || e.which;
          var has = $('.hasFocus')[0];
          var isFocused = document.activeElement == has;
          if ( e.shiftKey && !(isFocused && (has.nodeName == 'INPUT' || has.nodeName == 'TEXTAREA'))) {
            if (code == g.keys.right) {
              nav.right();
            } else if (code == g.keys.left) {
              nav.left();
            } else if (code == g.keys.up) {
              nav.up();
            } else if (code == g.keys.down) {
              nav.down();
            }
            e.stopPropagation();
          }
        }
      }
      
      MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
      var observer = new MutationObserver(function(mutations, observer) {
        //console.log(mutations, observer);
        $('*').off('focus', giveFocusEvent);
        $('*').on('focus', giveFocusEvent);
        giveFocus($(document.activeElement));
      });
      observer.observe(document, {
        subtree: true,
        childList: true
      });
      
      function init() {
        var ret;

        var first = $('.hasFocus');
        if (first.length != 1) {
          first = $('.target-first');
        }

        ret = g.newSpacialNav(settings.selector, first);
        $(document).on('keypress', handleNav(ret));

        return ret;
      }

      var fs = init();
      $('*').focus(giveFocusEvent);
    },

    newSpacialNav : function(selectors, startingTarget) { 
      
      function findBestChoice(target, choices) {
        var bias = 4;
        var coneBias = 1; // 1 = 90 degree cone, 0 == 180 degree screen
                          // partition, 2 == 45 degree cone, 4 == 22 degree cone

        var selfPos = getPosition(target);

        function selectMinFrom(set, valFn) {
          if (set.length == 0) {
            return target;
          } else {
            var best;
            var bestScore = null;

            $.each(set, function() {
              var score = valFn($(this));
              if (bestScore == null || score < bestScore) {
                bestScore = score;
                best = this;
              }
            });
            return best;
          }
        }

        function VDistance(other) {
          var oPos = getPosition(other);
          var ret = Math.abs(selfPos.y - oPos.y);
          return ret;
        };

        function HDistance(other) {
          var oPos = getPosition(other);
          var ret = Math.abs(selfPos.x - oPos.x);
          return ret;
        };

        function verticalBiasedDistance(other) {
          var ret = Math.sqrt(Math.pow(HDistance(other), 2)
              + Math.pow(VDistance(other) / bias, 2));
          return ret;
        };

        function horizontalBiasedDistance(other) {
          var ret = Math.sqrt(Math.pow(HDistance(other) / bias, 2)
              + Math.pow(VDistance(other), 2));
          return ret;
        };

        var upward = function(){
          var above = filter(choices, function(e) {
            var pos = getPosition(e);
            return pos.y < selfPos.y
                && (coneBias * VDistance(e)) > HDistance(e);
          });
          return selectMinFrom(above, verticalBiasedDistance);
        };

        var downward = function() {
          var below = filter(choices, function(e) {
            var pos = getPosition(e);
            return pos.y > selfPos.y
                && (coneBias * VDistance(e)) >= HDistance(e);
          });
          return selectMinFrom(below, verticalBiasedDistance);
        };

        var toLeft = function(){
          var leftOf = filter(choices, function(e) {
            var pos = getPosition(e);
            return pos.x < selfPos.x
                && (coneBias * HDistance(e)) >= VDistance(e);
          });
          return selectMinFrom(leftOf, horizontalBiasedDistance);
        };

        var toRight = function() {
          var rightOf = filter(choices, function(e) {
            var pos = getPosition(e);
            return pos.x > selfPos.x
                && (coneBias * HDistance(e)) >= VDistance(e);
          });
          return selectMinFrom(rightOf, horizontalBiasedDistance);
        };

        return [upward, toRight, downward, toLeft];
      };

      giveFocus(startingTarget);
      visual_debug(selectors);
      return {
        up : function() {
          console.log("Spacial nav up.");
          var target = $('.hasFocus');
          var transitions = findBestChoice(target, selectors());
          var transf = transitions[0];
          giveFocus(transf());
        },
        down : function() {
          console.log("Spacial nav down.");
          var target = $('.hasFocus');
          var transitions = findBestChoice(target, selectors());
          var transf = transitions[2];
          giveFocus(transf());
        },
        left : function() {
          console.log("Spacial nav left.");
          var target = $('.hasFocus');
          var transitions = findBestChoice(target, selectors());
          var transf = transitions[3];
          giveFocus(transf());
        },
        right : function() {
          console.log("Spacial nav right.");
          var target = $('.hasFocus');
          var transitions = findBestChoice(target, selectors());
          var transf = transitions[1];
          giveFocus(transf());
        }
      };
    }
  };
}

var gerkin = undefined;
if ($ != undefined) {
  gerkin = GerKiN($);
}
