#! /usr/bin/bash

rm -rf target
mkdir target
cat monkey_scripts/gerkin.monkey.header.txt > target/gerkin.monkey.user.js
cat assets/gerkin.js >> target/gerkin.monkey.user.js
